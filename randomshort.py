import webapp
import shelve
import random
import string

urls_db = shelve.open('urls_db')

class RandomShortApp(webapp.webApp):
    """Web application for shortening URLs with random resources"""
    """
    def __init__(self, hostname, port):
        super().__init__(hostname, port)
        self.urls_db = None  # Initialize as None
        self.start()  # Explicitly call start to initialize urls_db

    def start(self):
        "Open the shelve database upon starting the application"
        self.urls_db = shelve.open('urls_db')
        super().start()
    """
    def generate_random_resource(self):
        """Generate a random resource of length 6"""
        return ''.join(random.choices(string.ascii_lowercase + string.digits, k=6))

    def parse(self, request):
        """Parse the request and return relevant information"""
        lines = request.splitlines()
        firstLineWords = lines[0].split(' ', 2)
        verb = firstLineWords[0]
        resource = firstLineWords[1]
        separator = lines.index('')
        try:
            bodyFirstLine = lines[separator + 1]
        except IndexError:
            bodyFirstLine = None
        return (verb, resource, bodyFirstLine)

    def process(self, reqData):
        """Process the relevant elements of the request"""

        if urls_db is None:
            return ("500 Internal Server Error", "<html><body>Server error. Please try again later.</body></html>")

        (verb, resource, reqBody) = reqData

        if verb == "GET":
            if resource == "/":
                urls_list = "<br>".join([f"{short_url} - {urls_db[short_url]}" for short_url in urls_db.keys()])
                htmlBody = f"""
                <form action="/" method="post">
                URL to shorten: <input type="text" name="url">
                <input type="submit" value="Submit">
                </form>
                <br>
                <strong>Shortened URLs:</strong><br>
                {urls_list}
                """
                httpCode = "200 OK"
            elif resource in urls_db:
                # Redirect to the real URL for known resources
                real_url = urls_db[resource]
                return ("302 Found", f"Location: {real_url}\n")
            else:
                htmlBody = "Resource not available"
                httpCode = "404 Not Found"
        elif verb == "POST":
            params = reqBody.split('&')
            url = None
            for param in params:
                (name, value) = param.split("=", 1)
                if name == "url":
                    url = value
                    break

            if url:
                # Add the URL to the database with a random resource
                random_resource = self.generate_random_resource()
                urls_db[random_resource] = url
                htmlBody = f"Shortened URL: {random_resource}<br>Original URL: {url}"
                httpCode = "200 OK"
            else:
                htmlBody = "Error: Missing 'url' parameter in the request"
                httpCode = "400 Bad Request"

        return (httpCode, f"<html><body>{htmlBody}</body></html>")

    def shutdown(self):
        """Close the shelve database upon shutdown"""
        if urls_db is not None:
            urls_db.close()
        super().shutdown()

if __name__ == "__main__":
    randomShortApp = RandomShortApp("localhost", 1234)
    randomShortApp.handler()  # Start the application